import matplotlib.pyplot as plt
import numpy as np
import math
import pylab

f2 = open('times_scratch_mvapich.txt', 'r')
lines = f2.readlines()
f2.close()

x1 = []
y1 = []

for line in lines:
    p = line.split()
    x1.append(float(p[0]))
    y1.append(float(p[1]))   
xv = np.array(x1)
yv = np.array(y1)

f3 = open('times_scratch_t3pio.txt', 'r')
lines3 = f3.readlines()
f3.close()

x2 = []
y2 = []

for line in lines3:
    p = line.split()
    x2.append(float(p[0]))
    y2.append(float(p[1]))   
xv2 = np.array(x2)
yv2 = np.array(y2)

f4 = open('times_scratch_intelmpi.txt', 'r')
lines4 = f4.readlines()
f4.close()

x3 = []
y3 = []

for line in lines4:
    p = line.split()
    x3.append(float(p[0]))
    y3.append(float(p[1]))   
xv3 = np.array(x3)
yv3 = np.array(y3)


f5 = open('times_scratch_adios.txt', 'r')
lines5 = f5.readlines()
f5.close()

x4 = []
y4 = []

for line in lines5:
    p = line.split()
    x4.append(float(p[0]))
    y4.append(float(p[1]))   
xv4 = np.array(x4)
yv4 = np.array(y4)

f6 = open('times_scratch_sion.txt', 'r')
lines6 = f6.readlines()
f6.close()

x5 = []
y5 = []

for line in lines6:
    p = line.split()
    x5.append(float(p[0]))
    y5.append(float(p[1]))   
xv5 = np.array(x5)
yv5 = np.array(y5)

# xint = range(int(min(xv)), int(math.ceil(max(xv)))+1)
# plt.xticks(xint)

plt.grid()

plt.plot(xv, yv, '-o',label='mvapich_io')
plt.plot(xv2, yv2, '-o',label='t3pio')
plt.plot(xv3, yv3, '-o',label='intelmpi_io')
plt.plot(xv4, yv4, '-o',label='adios_io')
plt.plot(xv5, yv5, '-o',label='sion_io')

plt.legend(loc='upper left',prop={'size':14},labelspacing=0)

plt.suptitle('Timing on different nr of nodes on Stampede, \n each proc writes 4E08 bytes',fontsize=14)
plt.xlabel('Nr of nodes (16 proc each)',fontsize=14)
plt.ylabel('Total writing time (sec)',fontsize=14)

ax = pylab.gca()
ax.xaxis.set_visible(True)
ax.yaxis.set_visible(True)

for tick in ax.xaxis.get_major_ticks():
  tick.label.set_fontsize(16) 

for tick in ax.yaxis.get_major_ticks():
  tick.label.set_fontsize(16) 

plt.savefig('timing_mpi_io_nodes10to100_stampede_adios_sion.png',dpi=100)  
plt.show()

