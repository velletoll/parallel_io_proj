#!/bin/bash 

# This is for Stampede

module swap mvapich2 impi

# To compile MPI code
rm -f io_impi
mpif90 mpi_io.f90 -o io_impi

