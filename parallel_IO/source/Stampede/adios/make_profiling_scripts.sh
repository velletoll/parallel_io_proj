#!/bin/bash

# adios needed to be run in the same directory because of unresolved linking issues
# http://www.umbc.edu/hpcf/resources-tara-2013/scripting-perf-studies.html


# This function writes a SLURM script. We can call it with different parameter 
# settings to create different experiments

function write_script
{
    STUDY_NAME=$(printf 'study_nodes%03d' ${NODES})
    DIR_NAME=$(printf '%s' ${STUDY_NAME})

    cat << _EOF_ > ./run.slurm${DIR_NAME}
#!/bin/bash
#SBATCH -A TG-CCR140008
#SBATCH -J io_adios${DIR_NAME}
#SBATCH -o io_adios${DIR_NAME}.stdout
#SBATCH -N ${NODES} -n ${NCPUS}        
#SBATCH -p normal
#SBATCH -t 00:30:00   
export OUTFILE=valjund${NODES}.bp
export LD_LIBRARY_PATH=/home1/02875/vellet/katse/adios_2.5/source/adios-1.6.0/examples/Fortran/global-array/:$LD_LIBRARY_PATH # path to liblustreapi.so
ibrun ./adios_global           

_EOF_

    chmod 775 ./run.slurm${DIR_NAME}

}

# run the experiment with 1-4 nodes
for NODES in 10 20 30 40 50 60 70 80 90 100
  do
    NCPUS=$(($NODES * 16))
    write_script
  done
