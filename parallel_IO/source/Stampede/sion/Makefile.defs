#------------------------------------------------------------------------------
# SIONlib Makefile Definitions (Makefile.defs.linux-intel)
#------------------------------------------------------------------------------

CFGKEY = linux-intel-mpich2
CONFIG = Linux cluster with Intel compilers and mpich2 MPI

#------------------------------------------------------------------------------
# Platform Settings
#------------------------------------------------------------------------------
PREC   = 64
PFLAG  =
AFLAG  =

PREFIX = /home1/02875/vellet/katse/sionlib_fortran/sionlib_install

PLAT     = linux
#PLAT_LDIR   = -DPFORM_LDIR=\"/tmp\"
MACHINE  = -DMACHINE=\"Linux\ Intel\"
PLAT_CONF= $(MACHINE) $(PLAT_LDIR)
#PLATCC   = mpicc -cc="$(CC)"
PLATCC   = $(MPICC)

#------------------------------------------------------------------------------
# SIONlib General Settings
#------------------------------------------------------------------------------
LARGEFILE=-D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D__USE_LARGEFILE64 -D__USE_FILE_OFFSET64
OPTFLAGS = -g
CC       = icc
CFLAGS   = $(OPTFLAGS) $(SION_PLATFORM) $(LARGEFILE)
CXX      = icpc
CXXFLAGS = $(OPTFLAGS) $(SION_PLATFORM) -O3 -mieee-fp -wd161 $(LARGEFILE)
F77      = ifort
#F77      = ifc    #Intel 7.x or older
F90      = $(F77)
FFLAGS   = $(OPTFLAGS)
FPP      = -fpp2
FDOPT    = -D
LDFLAGS  = $(OPTFLAGS) 


#------------------------------------------------------------------------------
# SIONlib Settings
#------------------------------------------------------------------------------
SION_PLATFORM        = -D_SION_LINUX 
ARCH                 = LINUX

SION_LIBNAME_COM     = sioncom_$(PREC)
SION_LIBNAME_SER     = sionser_$(PREC)
SION_LIBNAME_GEN     = siongen_$(PREC)
SION_LIBNAME_OMP     = sionomp_$(PREC)
SION_LIBNAME_MPI     = sionmpi_$(PREC)
SION_LIBNAME_OMPI    = sionompi_$(PREC)

FORTRANENABLE        = 1
FORTRANNAMES         = -D_FORTRANUNDERSCORE
SION_LIBNAME_F77_SER = sionser_f77_$(PREC)
SION_LIBNAME_F77_MPI = sionmpi_f77_$(PREC)
SION_LIBNAME_F90_SER = sionser_f90_$(PREC)
SION_LIBNAME_F90_MPI = sionmpi_f90_$(PREC)
SION_MODPATH = mod_$(PREC)

CXXENABLE            = 1
SION_LIBNAME_CXX_SER = sionser_cxx_$(PREC)
SION_LIBNAME_CXX_MPI = sionmpi_cxx_$(PREC)

SION_DEBUG           = 

#------------------------------------------------------------------------------
# MPI Settings
#------------------------------------------------------------------------------


MPIENABLE = 1
MPICC     = mpicc -cc="$(CC)"
MPICXX    = mpicxx -cxx="$(CXX)"
MPIF77    = mpif77 -f77="$(F77)"
MPIF90    = mpif90 -f90="$(F90)"
MPILIB    = 
MPITESTRUN = mpiexec -np 4 --envall


#------------------------------------------------------------------------------
# OpenMP Settings
#------------------------------------------------------------------------------

OMPENABLE  = 1
OMPCC      = $(CC)
OMPCXX     = $(CXX)
OMPF77     = $(F77) 
OMPF90     = $(F90) 
OMPFLAG    = -openmp

#------------------------------------------------------------------------------
# Hybrid MPI/OpenMP Settings
#------------------------------------------------------------------------------

HYBENABLE  = 1
HYBCC      = $(MPICC)
HYBCXX     = $(MPICXX)
HYBF77     = $(MPIF77)
HYBF90     = $(MPIF90)
HYBFLAG    = $(OMPFLAG)

#------------------------------------------------------------------------------
# Compression Support
#------------------------------------------------------------------------------

SZLIB          = szlib
SZLIB_OPTFLAGS = -O3
SZLIB_CFLAGS   = -I$(TOPDIR)/utils/szlib -DELG_COMPRESSED -DCUBE_COMPRESSED
SZLIB_LIBPATH  = -L$(TOPDIR)/utils/szlib
SZLIB_LIB      = -lsc.z


#------------------------------------------------------------------------------
# SIONlib Cache Settings
#------------------------------------------------------------------------------

CACHEFLAGS=
CACHELIB=
#CACHEFLAGS=-D_SION_CACHE
#CACHELIB=-lrt
SION_PTHREADS = -DSION_PTHREADS
PTHREADSUPPORT = yes 
SION_LIBNAME_COM_LOCK_PTHREADS = $(SION_LIBNAME_COM)_lock_pthreads
SION_LIBNAME_COM_LOCK_NONE = $(SION_LIBNAME_COM)_lock_none
SION_DEBUG = 

#------------------------------------------------------------------------------
# GNU Make Support
#-----------------------------------------------------------------------------

MAKE = make
