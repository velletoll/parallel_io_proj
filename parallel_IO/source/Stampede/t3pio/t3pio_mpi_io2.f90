
! example of parallel MPI write into a single file, in Fortran

PROGRAM main
    ! Fortran 90 users can (and should) use
    !     use mpi
    ! instead of include 'mpif.h' if their MPI implementation provides a
    ! mpi module.
    use mpi
	use t3pio
    
	! thefile is handle to file
    integer ierr, i, myrank, BUFSIZE, thefile
    parameter (BUFSIZE=100000000)
    integer buf(BUFSIZE)
	! double precision reals for timing
	real(kind=8) :: pretim, elapsetime, entim
	! tells the place where to write in file
    integer(kind=MPI_OFFSET_KIND) disp
	integer, parameter :: out_unit=20
	
	! variables for t3pio
	character*(256) :: dir      ! directory of output file
    integer info                ! MPI Info object
    integer iTotalSz            ! File size in MB.

	
    call MPI_INIT(ierr)
    call MPI_COMM_RANK(MPI_COMM_WORLD, myrank, ierr)

	! fill array with some nrs 
    do i = 0, BUFSIZE
        buf(i) = myrank * BUFSIZE + i
    enddo
	
	
    call MPI_Info_create(info,ierr)

    iTotalSz = 4*BUFSIZE*NPROCS / (1024*1024)
    dir      = "./"
    call t3pio_set_info(MPI_COMM_WORLD, info, dir, ierr,   &
                       global_size = iTotalSz)
	
	! Start timing before opening file
	pretim = MPI_Wtime()
    
	call MPI_FILE_OPEN(MPI_COMM_WORLD, 'testfile', &
                       MPI_MODE_WRONLY + MPI_MODE_CREATE, &
                       info, thefile, ierr)
					   
    ! assume 4-byte integers
	! this tells how far to write
    disp = myrank * BUFSIZE * 4
    
	!http://www.mpi-forum.org/docs/mpi-2.2/mpi22-report/node274.htm#Node274
	call MPI_FILE_SET_VIEW(thefile, disp, MPI_INTEGER, &
                           MPI_INTEGER, 'native', &
                           info, ierr)
						   
    call MPI_FILE_WRITE(thefile, buf, BUFSIZE, MPI_INTEGER, &
                        MPI_STATUS_IGNORE, ierr)
						
    call MPI_FILE_CLOSE(thefile, ierr)
	! end timing after closing the file
	entim = MPI_Wtime()
    elapsetime=entim-pretim

	if(myrank.eq.0) then
      print*,'elapsed time is', elapsetime,' seconds'
	  open (unit=out_unit,file="./time.txt",action="write",status="replace")
	  write (out_unit,*) elapsetime
	  close (out_unit)
    end if
	
    call MPI_FINALIZE(ierr)

END PROGRAM main
