#!/bin/bash

# http://www.umbc.edu/hpcf/resources-tara-2013/scripting-perf-studies.html

EXECUTABLE='/scratch/02875/vellet/project/parallel_computing_project/Velle/parallel_io_profiling_Stampede/mvapich_io/io_mvapich'

# This function writes a SLURM script. We can call it with different parameter 
# settings to create different experiments

function write_script
{
    STUDY_NAME=$(printf 'study_nodes%03d' ${NODES})
    DIR_NAME=$(printf '%s' ${STUDY_NAME})

    if [ -d $DIR_NAME ] ; then
        echo "$DIR_NAME already exists, skipping..."
        return 0
    else
        echo "Creating job $DIR_NAME"
    fi

    mkdir -p $DIR_NAME

    cat << _EOF_ > ${DIR_NAME}/run.slurm
#!/bin/bash
#SBATCH -A TG-CCR140008
#SBATCH -J io_mvapich
#SBATCH -o io_mvapich.stdout
#SBATCH -N ${NODES} -n ${NCPUS}        
#SBATCH -p normal
#SBATCH -t 00:20:00   
cd /scratch/02875/vellet/project/parallel_computing_project/Velle/parallel_io_profiling_Stampede/mvapich_io/$DIR_NAME
ibrun ./io_mvapich
_EOF_

    chmod 775 ${DIR_NAME}/run.slurm
    ln -s ${EXECUTABLE} ${DIR_NAME}/
}

# run the experiment with 1-4 nodes
for NODES in 10 20 30 40 50 60 70 80 90 100
  do
    NCPUS=$(($NODES * 16))
    write_script
  done
