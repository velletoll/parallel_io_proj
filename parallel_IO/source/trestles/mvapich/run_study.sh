#!/bin/bash

for i in ./study_nodes*;
  do
    cd $i; sbatch run.slurm; cd ../;
  done